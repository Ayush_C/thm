# Nmap_Scan

```
# Nmap 7.91 scan initiated Thu May 13 07:32:47 2021 as: nmap -vv -A -O -oN nmap/output 10.10.174.53
Increasing send delay for 10.10.174.53 from 0 to 5 due to 85 out of 283 dropped probes since last increase.
Increasing send delay for 10.10.174.53 from 5 to 10 due to 169 out of 563 dropped probes since last increase.
Nmap scan report for 10.10.174.53
Host is up, received echo-reply ttl 127 (0.19s latency).
Scanned at 2021-05-13 07:32:48 EDT for 121s
Not shown: 991 closed ports
Reason: 991 resets
PORT      STATE SERVICE            REASON          VERSION
135/tcp   open  msrpc              syn-ack ttl 127 Microsoft Windows RPC
139/tcp   open  netbios-ssn        syn-ack ttl 127 Microsoft Windows netbios-ssn
445/tcp   open  microsoft-ds       syn-ack ttl 127 Windows 7 Professional 7601 Service Pack 1 microsoft-ds (workgroup: WORKGROUP)
3389/tcp  open  ssl/ms-wbt-server? syn-ack ttl 127
| ssl-cert: Subject: commonName=Jon-PC
| Issuer: commonName=Jon-PC
| Public Key type: rsa
| Public Key bits: 2048
| Signature Algorithm: sha1WithRSAEncryption
| Not valid before: 2021-05-12T11:29:42
| Not valid after:  2021-11-11T11:29:42
| MD5:   8207 da9e 71c9 5e4f f577 3f82 b3c6 f740
| SHA-1: 64eb ed79 e1be 76e6 5083 7489 1d8c 6725 fac4 1452
| -----BEGIN CERTIFICATE-----
| MIIC0DCCAbigAwIBAgIQKGqnE/B6fq9ECjwW9V6JFDANBgkqhkiG9w0BAQUFADAR
| MQ8wDQYDVQQDEwZKb24tUEMwHhcNMjEwNTEyMTEyOTQyWhcNMjExMTExMTEyOTQy
| WjARMQ8wDQYDVQQDEwZKb24tUEMwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEK
| AoIBAQDr/k3D5zpEfipauABeFv6sm1vn3iHpdQ4tr2ycxJ3jNp14OSxjTke0n1Hm
| Gwzn5WaCvneNzIkDOSPywMFeicvX1IwaBc+3Uv0PMIm0NzJNN2PPHmwmHUDWXaSq
| X0PdpbfFo1CMpc8ScOOruOrIWRkgKgYS98bvLUk7dXTvrZqm644anVkA0Ill/MSS
| lBN3zMEptT7jAgLtVeZfz+PVUvTFGurcRybR5gD6xwtd9As/ojmhwnrD2c7hmRPN
| NaF2DvCnYwZiQYFn/uiaKmC5CW+c3piUQ84Ngf2utNaM6KtnrtIECMjcA7H3B1Nk
| WZM8F6hRuH+TvaedVOYy7rryGupXAgMBAAGjJDAiMBMGA1UdJQQMMAoGCCsGAQUF
| BwMBMAsGA1UdDwQEAwIEMDANBgkqhkiG9w0BAQUFAAOCAQEAXagfB+CNWMSCjaKE
| ZKc+Y3+0XBBHTMLCwRlRTV5yKT8dgSYe6136LqBFrDT+rBQYbxkFIZOIx53/B/Is
| pgnW9eR7YJF38trp6JLlkTrTR2j7NBJpmjXG33PvUkqbekSXybVem7iVgpTosOCE
| RTO85Lc1eTrJXu2E10FLSsDKq2TgwPOchF595HjlVOPiklPJnVnJVps3tJCGVe+g
| /TRONsWrEtXK43u40KhGqSDaIkFUxrqEsth3C4TlA1bXfeISKUaLO4B8LSw1Zn4x
| CrYY0M9RePZHrJ9mO+dzMJc2H9nAHIDXWY165z35dMUku475AgSlHU44u0lUHJeh
| oAm1kQ==
|_-----END CERTIFICATE-----
|_ssl-date: 2021-05-13T11:34:48+00:00; 0s from scanner time.
49152/tcp open  msrpc              syn-ack ttl 127 Microsoft Windows RPC
49153/tcp open  msrpc              syn-ack ttl 127 Microsoft Windows RPC
49154/tcp open  msrpc              syn-ack ttl 127 Microsoft Windows RPC
49158/tcp open  msrpc              syn-ack ttl 127 Microsoft Windows RPC
49159/tcp open  msrpc              syn-ack ttl 127 Microsoft Windows RPC
No exact OS matches for host (If you know what OS is running on it, see https://nmap.org/submit/ ).
TCP/IP fingerprint:
OS:SCAN(V=7.91%E=4%D=5/13%OT=135%CT=1%CU=37257%PV=Y%DS=2%DC=T%G=Y%TM=609D0E
OS:D9%P=x86_64-pc-linux-gnu)SEQ(SP=106%GCD=1%ISR=10C%TI=I%CI=I%II=I%SS=S%TS
OS:=7)SEQ(SP=107%GCD=1%ISR=10C%TI=I%CI=I%TS=7)OPS(O1=M506NW8ST11%O2=M506NW8
OS:ST11%O3=M506NW8NNT11%O4=M506NW8ST11%O5=M506NW8ST11%O6=M506ST11)WIN(W1=20
OS:00%W2=2000%W3=2000%W4=2000%W5=2000%W6=2000)ECN(R=Y%DF=Y%T=80%W=2000%O=M5
OS:06NW8NNS%CC=N%Q=)T1(R=Y%DF=Y%T=80%S=O%A=S+%F=AS%RD=0%Q=)T2(R=Y%DF=Y%T=80
OS:%W=0%S=Z%A=S%F=AR%O=%RD=0%Q=)T3(R=Y%DF=Y%T=80%W=0%S=Z%A=O%F=AR%O=%RD=0%Q
OS:=)T4(R=Y%DF=Y%T=80%W=0%S=A%A=O%F=R%O=%RD=0%Q=)T5(R=Y%DF=Y%T=80%W=0%S=Z%A
OS:=S+%F=AR%O=%RD=0%Q=)T6(R=Y%DF=Y%T=80%W=0%S=A%A=O%F=R%O=%RD=0%Q=)T7(R=Y%D
OS:F=Y%T=80%W=0%S=Z%A=S+%F=AR%O=%RD=0%Q=)U1(R=Y%DF=N%T=80%IPL=164%UN=0%RIPL
OS:=G%RID=G%RIPCK=G%RUCK=G%RUD=G)IE(R=Y%DFI=N%T=80%CD=Z)

Uptime guess: 0.004 days (since Thu May 13 07:28:35 2021)
Network Distance: 2 hops
TCP Sequence Prediction: Difficulty=263 (Good luck!)
IP ID Sequence Generation: Incremental
Service Info: Host: JON-PC; OS: Windows; CPE: cpe:/o:microsoft:windows

Host script results:
|_clock-skew: mean: 1h15m00s, deviation: 2h30m00s, median: 0s
| nbstat: NetBIOS name: JON-PC, NetBIOS user: <unknown>, NetBIOS MAC: 02:1b:d8:42:89:69 (unknown)
| Names:
|   JON-PC<00>           Flags: <unique><active>
|   WORKGROUP<00>        Flags: <group><active>
|   JON-PC<20>           Flags: <unique><active>
|   WORKGROUP<1e>        Flags: <group><active>
|   WORKGROUP<1d>        Flags: <unique><active>
|   \x01\x02__MSBROWSE__\x02<01>  Flags: <group><active>
| Statistics:
|   02 1b d8 42 89 69 00 00 00 00 00 00 00 00 00 00 00
|   00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
|_  00 00 00 00 00 00 00 00 00 00 00 00 00 00
| p2p-conficker: 
|   Checking for Conficker.C or higher...
|   Check 1 (port 11878/tcp): CLEAN (Couldn't connect)
|   Check 2 (port 17701/tcp): CLEAN (Couldn't connect)
|   Check 3 (port 18858/udp): CLEAN (Timeout)
|   Check 4 (port 37273/udp): CLEAN (Failed to receive data)
|_  0/4 checks are positive: Host is CLEAN or ports are blocked
| smb-os-discovery: 
|   OS: Windows 7 Professional 7601 Service Pack 1 (Windows 7 Professional 6.1)
|   OS CPE: cpe:/o:microsoft:windows_7::sp1:professional
|   Computer name: Jon-PC
|   NetBIOS computer name: JON-PC\x00
|   Workgroup: WORKGROUP\x00
|_  System time: 2021-05-13T06:34:42-05:00
| smb-security-mode: 
|   account_used: guest
|   authentication_level: user
|   challenge_response: supported
|_  message_signing: disabled (dangerous, but default)
| smb2-security-mode: 
|   2.02: 
|_    Message signing enabled but not required
| smb2-time: 
|   date: 2021-05-13T11:34:43
|_  start_date: 2021-05-13T11:29:32

TRACEROUTE (using port 143/tcp)
HOP RTT       ADDRESS
1   284.14 ms 10.9.0.1
2   284.65 ms 10.10.174.53

Read data files from: /usr/bin/../share/nmap
OS and Service detection performed. Please report any incorrect results at https://nmap.org/submit/ .
# Nmap done at Thu May 13 07:34:49 2021 -- 1 IP address (1 host up) scanned in 122.40 seconds
```
